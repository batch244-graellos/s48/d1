// console.log("Hello");

// Mock database
let posts =[]

// Post ID
let count = 1;

// Add post
// This will trigger an event that will add a new post in our mock database upon clicking the "Create" button.

document.querySelector("#form-add-post").addEventListener("submit", (event) => {

	// Prevents the page from reloading
	event.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	})

	// count will increment everytime a post is created
	count++;

	alert("Successfully added!")
	showPost(posts);
});

// Show posts
const showPost = (posts) => {

	// Variable that will contain all the posts
	let postEntries = "";

	posts.forEach((post) => {

		console.log(post);

		// We can assign HTML elements in JS Variables
		postEntries += 
		`<div id ="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onclick="editPost('${post.id}')">Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>
		</div>`


	})

	// To display the posts in our HTML document
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// Edit post
// This will trigger an event that will update a certain POST upon clicking the edit button.

/*
    Transfer the id, title and body of each post entry to the second form when the edit button has been clicked:
        -  the id of the div will be copied to the hidden input field of Edit Post form
        - the title of the div will be copied to the title input field of Edit Post form
        - the body of the div will be copied to the body input field of Edit Post form
*/

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
}

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

	e.preventDefault();


// Loops through every post in the "posts" array starting from the first element/indorx 0
	for(let i=0; i < posts.length; i++) {

		// The value of posts[i].id is a Number while document.querySelector('#txt-edit-id').value is a String
		// Therefore, it is necessary to convert Number to a String first
		if (posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {

			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value;

			showPost(posts);
			alert("Successfully updated");

			break;
		}

	}
})

// ACTIVITY - Delete a post
function deletePost(postId) {
  const deletePost = document.querySelector(`#post-${postId}`);
  deletePost.remove();

}


